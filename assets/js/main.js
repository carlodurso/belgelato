(function($) {

	$(function() {

			var	$window = $(window),
					$body = $('body'),
					$header = $('header'),
					$menu = $('#menu li'),
					$footer = $('footer'),
					$parallax = $('.parallax'),
					today = new Date(),
					year = today.getFullYear();

			// Disable animations/transitions until the page has loaded.
			$body.addClass('is-loading dom-ready');


			var openingHours = function(){

				// Calculating Opening Hours
					var Dates = [],
					lastDay = [31,28,31,30,31,30,31,31,30,31,30,31];

					if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)){
						lastDay[2] = 29;
					}

					for (var date = new Date(), month=0; month<12; month+=1) {
						date.setFullYear(year, month, lastDay[month]);
						date.setDate(date.getDate()-date.getDay());

						if(month === 2 || month === 9){
							Dates.push(date.toISOString().substring(0,10));
						}
					}

					var march = new Date(Dates[0]);
					var october = new Date(Dates[1]);
					var dst = today > march && today < october ? true : false;

					return dst ? "10am - 6pm" : "9am - 5pm";
			}


			var getCurrentSeason = function (today) {
					var currentYear = today.getFullYear();

					if (today < new Date(currentYear, 2, 21)) {
						// [Jan 1 ~ Mar 20)
						return 'winter';
					}
					if (today < new Date(currentYear, 5, 21)) {
						// [Mar 21 ~ Jun 20)
						return 'spring';
					}
					if (today < new Date(currentYear, 8, 21)) {
						// [Jun 21 ~ Sep 20)
						return 'summer';
					}
					if (today < new Date(currentYear, 11, 21)) {
						// [Sep 21 ~ Dec 20)
						return 'autumn';
					}

					return 'winter';
				}

				var remoteUrl = "//belgelato.herokuapp.com/products",
						localURL = 'assets/json/belGelato.json';


				var checkUrl = function(url){
					$.ajax({
						url : url,
						success: function(data, textStatus, xhr) {
								// return xhr.status;
								getProductListing(data);
						},
						error: function(xhr, textStatus) {

								$.getJSON( localURL, function() {
								}).done(function(localJSON) {
    								getProductListing(localJSON);
  							})
						}
					});
				};

				var getProductListing = function(data){

					var object = [],
							page,
							label,
							index,
							content,
							$container;

							var $heading,
									$image,
									is_mobile = $(window).width() < 650,
									mobile_view;

					for (var product in data) {

						if (data.hasOwnProperty(product)) {
							if(window.location.href.indexOf(data[product].page.toLowerCase()) > -1) {

								page = data[product].page.toLowerCase();
								label = data[product].label.toLowerCase();

								object.push(data[product].items);

								$container = $('#' + label + '-list');
								$container.empty();

								var items = object[0].map(function (item) {
									$heading = '<h4>' + item.name + '</h4>\n',
									$image = '<img src="/images/' + page + '/' + item.image + '" alt="' + item.name + '">\n';
									mobile_view = is_mobile ? $heading + $image : $image + $heading;

									return '<div>\n' +
													 mobile_view +
													'<p class="description">' + item.description + '</p>\n' +
													'</div>\n';
								});

								if (items.length) {
									content = '<li class="item">' + items.join('</li>\n<li class="item">') + '</li>';
								}

								$container.append(content);
								object.shift();
							}
						}
					}
			}


			var cookieconsent = function(){
					window.cookieconsent.initialise({
						"palette": {
							"popup": {
								"background": "#4C3122"
							},
							"button": {
								"background": "#f1d600"
							}
						},
						"position": "bottom",
						// "static": true,
						"content": {
							"message": "This website uses cookies to enhance your experience.",
							"href": "http://ec.europa.eu/ipg/basics/legal/cookies/index_en.htm"
						}
					})
			};

			// Adding and removing information on the loaded page
			$window.on('load', function() {
				checkUrl(remoteUrl);

				window.setTimeout(function() {
					$body.removeClass('is-loading');

					$footer.load("footer.html", function() {
						$('.open-hours').text(openingHours);
					  $('.year').text(year);
						cookieconsent();
					});

				}, 100);

				$('.owl-carousel').owlCarousel({
						loop:true,
						margin:0,
						nav:false,
						dots:false,
						autoplay: true,
						responsive:{
								0:{
										items:1
								},
								600:{
										items:2
								},
								1200:{
										items:3
								},
								1500:{
										items:4
								}
						}
				});
			});

		// Active url
			$menu.each(function(){
        var a_href = $(this).find('a');

				if(window.location.href.indexOf(a_href.attr('href')) > -1) {
				 	a_href.parents('li').addClass('active');
				} else {
					$('.home').parents('li').addClass('active');
				}
     });


		 if( $(window).width() < 650) {

			 $('.active').attr('data-sort', '1');

			 var list = $('#menu');
			 var listItems = list.find('li').sort(function(a,b){ return $(a).attr('data-sort') - $(b).attr('data-sort'); });
			 list.find('li').remove();
			 list.append(listItems);
			 list.find('li a').addClass('navigation');

			 $('.icon').on('click', function(e) {
				 $('#menu').toggleClass("responsive"); //you can list several class names
				 e.preventDefault();
			 });

			 $body.on('click', function(e) {
				 if(!$(event.target).hasClass('navigation')){
					 $('#menu').removeClass('responsive');
				 }
			});
		 }

		// Posts.
			var $posts = $('.post');

			$posts.each(function() {

				var $this = $(this),
						$image = $this.find('.image'),
						$img = $image.find('img'),
						x;

					// Set image.
						if ($img.attr('src') !== undefined)
							$image.css('background-image', 'url(' + $img.attr('src') + ')');
							// $image.css('height', $img.height());
							// $image.css('background-repeat', 'no-repeat');
							$img.css('display','none');

					// Set position.
						if (x = $img.data('position'))
							$image.css('background-position', x);

			});

				$window.bind("resize scroll",function(e) {
				  var y = $window.scrollTop();
					var x = $window.scrollLeft();

					if ($window.scrollTop() >= 70) {
			        $("#menu").addClass("scrolling");
			    } else {
			        $("#menu").removeClass("scrolling");
			    }

					var smartphones = $(window).width() < 650 ? 1.8 : 2.5;
					var value = $(window).width() > 1025 ? 1.8 : smartphones;

			    $parallax.filter(function() {
			        return $(this).offset().top < (y + $(window).height()) &&
			               $(this).offset().top + $(this).height() > y;
			    }).css('background-position', '50% ' + parseInt(-y / value) + 'px');
				});
	});

	$.fn.isOnScreen = function(){

    var win = $(window);

    var viewport = {
        top : win.scrollTop(),
        left : win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();

    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();

    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

	};

})(jQuery);
