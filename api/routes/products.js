var express = require('express');
var router = express.Router();
var Products = require('../models/Products.js');


/* GET /products listing. */
router.get('/', function(req, res, next) {

  Products.find(function (err, products) {
    if (err) return next(err);

    res.json(products);
  });
});

/* POST /products */
router.post('/', function(req, res, next) {
  Products.create(req.body, function (err, product) {
    if (err) return next(err);
    res.json(product);
  });
});

/* GET /products/id */
router.get('/:id([0-9a-f]{24})', function(req, res, next) {
  var id = req.params.id;
  Products.findById(id, function (err, product) {
    if (err) return next(err);
    res.json(product);
  });
});

/* GET /products/category */
router.get('/:category', function(req, res, next) {
  var category = req.params.category.toLowerCase();
  Products.find({label:category}, function (err, products) {
    if (err) return next(err);
    res.json(products[0].items);
  });
});


/* GET /products/category/available */
router.get('/:category/available', function(req, res, next) {
  var category = req.params.category.toLowerCase();

  Products.find({label:category}, function (err, products) {
    if (err) return next(err);

    var available = products[0].items.filter(function (item) {
       if (item.available === true){
         return item;
      }
    });

    res.json(available);
  });
});

/* GET /products/category/style */
router.get('/:category/:style', function(req, res, next) {
  var category = req.params.category.toLowerCase();

  Products.find({label:category}, function (err, products) {
    if (err) return next(err);

    var style = products[0].items.filter(function (item) {
       if (item.style.indexOf(req.params.style) > -1){
         return item;
      }
    });

    res.json(style);
  });
});



/* DELETE /products/:id */
router.delete('/:id', function(req, res, next) {
  Products.findByIdAndRemove(req.params.id, req.body, function (err, product) {
    if (err) return next(err);
    res.json(product);
  });
});

module.exports = router;
