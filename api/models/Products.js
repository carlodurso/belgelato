var mongoose = require('mongoose');


var itemSchema = new mongoose.Schema({
    id: { type: Number, unique: true},
    name: { type: String, lowercase: true, trim: true},
    image: { type: String, lowercase: true, trim: true},
    description: String,
    style: [String],
    available: Boolean,
    updated_at: { type: Date, default: Date.now }
},{ _id : false });

var categorySchema = new mongoose.Schema({
    label: { type: String, lowercase: true, trim: true},
    page: { type: String, lowercase: true, trim: true},
    items: [itemSchema]
})

module.exports = mongoose.model('Products', categorySchema);
