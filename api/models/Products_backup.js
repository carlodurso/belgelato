var mongoose = require('mongoose');


var itemSchema = new mongoose.Schema({
    _id: { type: Number, unique: true},
    name: { type: String, trim: true},
    image: String,
    description: String,
    style: [String],
    available: Boolean,
    category: String,
    updated_at: { type: Date, default: Date.now }
}, {versionKey: false});


module.exports = mongoose.model('products', itemSchema);
