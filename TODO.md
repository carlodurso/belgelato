## What's missing?
 - Testimonials
 - Quick facts
 - Team
 - Videos
 - Press
 - Values
 - Whereabouts
 - Google Analytics

## General
  - Remove commented out code. ✓

## Footer
 - Check tooltips text in footer. ✓
 - Change social links URLs. ✓

## Index
 - Fix headers in mobile version. ✓
 - Change link for cookie policy. ✓
 - Change images. Less stock photos.
 - Change headers colors accordingly.
 - Change text to reflect each section. ✓
 - Change carousel images. Needs the addition of a json. ✓


## Flavours
  - Change hero image. ✓
  - Add real flavours. Needs json API. ✓
  - Add description to flavours.

## Food
  - Change hero image. ✓
  - Add real food items. Needs json API. ✓
  - Add description to items.
  - Button 'About us' overlaps footer. ✓

## Cakes
  - Change hero image. ✓
  - Add real food items. Needs json API. ✓
  - Add description to items.
  - Button 'About us' overlaps footer. ✓

## About
  - Change hero image. ✓
  - Change text to reflect each section. ✓
  - Change images. Less stock photos. ✓

## API
  - Set up Git on Heroku ✓
  - Create a fallback json ✓
  - Finalize the APIs for the products and carousel. ✓
