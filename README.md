# README #

This is the repo for Bel Gelato website.

### What is this repository for? ###

* Bel Gelato website
* v1.0

### How do I get set up? ###

#### Summary of set up
  It's mainly HTML. Can run in any browser.
  The API folder is based on Node and Express JS. You need to run NPM install in order to get the required modules. Mongo is setup in the cloud.

#### Configuration
#### Dependencies
#### Database configuration
#### How NOT to run tests
#### Deployment instructions

### Contribution guidelines ###

#### Writing tests
#### Code review
#### Other guidelines

### Who do I talk to? ###

#### Repo owner or admin
  Carlo D'Urso

#### Other community or team contact
